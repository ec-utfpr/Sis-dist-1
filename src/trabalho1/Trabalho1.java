package trabalho1;

import java.net.*;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.apache.commons.codec.binary.Base64;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;

//Encryption imports
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import static trabalho1.Trabalho1.portIsOpen;

/**
 *
 * @author Lucas Vale de Araujo
 * @author Gustavo Pereira de Morais
 */
    class PeerInfo
    {
        public PeerInfo(String publicKey, String IP, int socket, int reputacao)
        {
            this.publicKey = publicKey;
            this.IP = IP;
            this.socket = socket;
            this.reputacao = reputacao;

        }

        public String publicKey;
        public String IP;
        public int socket;
        public int reputacao;
        
        @Override
        public String toString(){
            return publicKey.substring(0,5) + ";" + IP+ ";" + socket + ";" + reputacao;
        }
    }

class GenerateKeys {

	private KeyPairGenerator keyGen;
	private KeyPair pair;
	private PrivateKey privateKey;
	private PublicKey publicKey;

	public GenerateKeys(int keylength) throws NoSuchAlgorithmException, NoSuchProviderException {
		this.keyGen = KeyPairGenerator.getInstance("RSA");
		this.keyGen.initialize(keylength);
	}

	public void createKeys() {
		this.pair = this.keyGen.generateKeyPair();
		this.privateKey = pair.getPrivate();
		this.publicKey = pair.getPublic();
	}

	public PrivateKey getPrivateKey() {
		return this.privateKey;
	}

	public PublicKey getPublicKey() {
		return this.publicKey;
	}

	public void writeToFile(String path, byte[] key) throws IOException {
		File f = new File(path);
		f.getParentFile().mkdirs();

		FileOutputStream fos = new FileOutputStream(f);
		fos.write(key);
		fos.flush();
		fos.close();
	}

	public static void main(String[] args) {
		GenerateKeys gk;
		try {
			gk = new GenerateKeys(1024);
			gk.createKeys();
			gk.writeToFile("KeyPair/publicKey", gk.getPublicKey().getEncoded());
			gk.writeToFile("KeyPair/privateKey", gk.getPrivateKey().getEncoded());
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

	}

}

class AsymmetricCryptography {
	private Cipher cipher;

	public AsymmetricCryptography() throws NoSuchAlgorithmException, NoSuchPaddingException {
		this.cipher = Cipher.getInstance("RSA");
	}

	// https://docs.oracle.com/javase/8/docs/api/java/security/spec/PKCS8EncodedKeySpec.html
	public PrivateKey getPrivate(String filename) throws Exception {
		byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	// https://docs.oracle.com/javase/8/docs/api/java/security/spec/X509EncodedKeySpec.html
	public PublicKey getPublic(String filename) throws Exception {
		byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

	public void encryptFile(byte[] input, File output, PrivateKey key) 
		throws IOException, GeneralSecurityException {
		this.cipher.init(Cipher.ENCRYPT_MODE, key);
		writeToFile(output, this.cipher.doFinal(input));
	}

	public void decryptFile(byte[] input, File output, PublicKey key) 
		throws IOException, GeneralSecurityException {
		this.cipher.init(Cipher.DECRYPT_MODE, key);
		writeToFile(output, this.cipher.doFinal(input));
	}

	private void writeToFile(File output, byte[] toWrite)
			throws IllegalBlockSizeException, BadPaddingException, IOException {
		FileOutputStream fos = new FileOutputStream(output);
		fos.write(toWrite);
		fos.flush();
		fos.close();
	}

	public String encryptText(String msg, PrivateKey key) 
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException, 
			BadPaddingException, InvalidKeyException {
		this.cipher.init(Cipher.ENCRYPT_MODE, key);
		return Base64.encodeBase64String(cipher.doFinal(msg.getBytes("UTF-8")));
	}

	public String decryptText(String msg, PublicKey key)
			throws InvalidKeyException, UnsupportedEncodingException, 
			IllegalBlockSizeException, BadPaddingException {
		this.cipher.init(Cipher.DECRYPT_MODE, key);
		return new String(cipher.doFinal(Base64.decodeBase64(msg)), "UTF-8");
	}

	public byte[] getFileInBytes(File f) throws IOException {
		FileInputStream fis = new FileInputStream(f);
		byte[] fbytes = new byte[(int) f.length()];
		fis.read(fbytes);
		fis.close();
		return fbytes;
	}

	/*public static void main(String[] args) throws Exception {
		AsymmetricCryptography ac = new AsymmetricCryptography();
		PrivateKey privateKey = ac.getPrivate("KeyPair/privateKey");
		PublicKey publicKey = ac.getPublic("KeyPair/publicKey");

                String publicK = Base64.encodeBase64String(publicKey.getEncoded());
                System.out.println(publicK.length());
                byte[] publicBytes = Base64.decodeBase64(publicK);
                X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                PublicKey pubKey = keyFactory.generatePublic(keySpec);

		String msg = "Cryptography is fun!";
		String encrypted_msg = ac.encryptText(msg, privateKey);
		String decrypted_msg = ac.decryptText(encrypted_msg, pubKey);
		System.out.println("Original Message: " + msg + 
			"\nEncrypted Message: " + encrypted_msg
			+ "\nDecrypted Message: " + decrypted_msg);

		if (new File("KeyPair/text.txt").exists()) {
			ac.encryptFile(ac.getFileInBytes(new File("KeyPair/text.txt")), 
				new File("KeyPair/text_encrypted.txt"),privateKey);
			ac.decryptFile(ac.getFileInBytes(new File("KeyPair/text_encrypted.txt")),
				new File("KeyPair/text_decrypted.txt"), pubKey);
		} else {
			System.out.println("Create a file text.txt under folder KeyPair");
		}
	}*/
}

public class Trabalho1 {
    /**
     * @param args the command line arguments, the args passed will be:
     * args[0] = multicast address
     * args[1] = port used for connect on multicast
     *
     */
    public static void main(String[] args) throws NoSuchAlgorithmException 
    {   
        //Creating the multicast
        MulticastSocket s =null;
	//Creating the localip string and socket
        String LocalIP;
        Integer mySocket = -1;
         
        List<PeerInfo> peerList = new ArrayList<>();
        
        GenerateKeys gk;
	try {
            gk = new GenerateKeys(1024);
            gk.createKeys();
            //System.out.println("My pub key: "+ gk.getPublicKey().toString());
            String publicK = Base64.encodeBase64String(gk.getPublicKey().getEncoded());
            try 
            {
                //Opening the multicast connection
                InetAddress group = InetAddress.getByName(args[0]);
                s = new MulticastSocket(Integer.parseInt(args[1]));
                //Joining the multicast group
                s.joinGroup(group);	
                
                
                //Finding the index of the bar so i can get the local ip adress in the string
                LocalIP = InetAddress.getLocalHost().toString().substring(
                        InetAddress.getLocalHost().toString().indexOf("/") + 1);
                UnicastReciver uReceiver;
                for(int i = 1000; i < 10000; i++){
                        try {
                            DatagramSocket aSocket = null;
                            aSocket = new DatagramSocket(i); 
                            aSocket.disconnect();
                            aSocket.close();
                            mySocket = i;
                            //Creating a thread to receive the answer from joining the network
                            uReceiver = new UnicastReciver(LocalIP, mySocket, peerList, 1, gk.getPrivateKey());
                            break;
                        } catch (Exception e) {
                        }
                    }
                
                
                //Creating a reader for get input from keyboard
                Scanner sc = new Scanner(System.in);
                
                
                //Formating the public key to be sent
                byte [] publicKey = ("1" + publicK + mySocket).getBytes();
                
                
                
                //Sending a message with the public key to everyone
                DatagramPacket messageOut;
                messageOut = new DatagramPacket(publicKey, publicKey.length,group, Integer.parseInt(args[1]));
                s.send(messageOut);
       
                //Creating a thread to read all the messages from the multicast
                listener mListener = new listener(s, peerList, publicK, gk.getPrivateKey());
                
                while(true)
                {                   
                    //Getting the input from keyboard
                    
                    String inputMessage = sc.nextLine();
                    
                    
                    //Checking if the input is a close command
                    if("sair".equals(new String(inputMessage).trim()))
                    {
                        mListener.exit(0);
                        break;
                    }
                    else
                    {
                    for(int i = 1000; i < 10000; i++){
                        try {
                            DatagramSocket aSocket = null;
                            aSocket = new DatagramSocket(i); 
                            aSocket.disconnect();
                            aSocket.close();
                            mySocket = i;
                            //Creating a thread to receive the answer from joining the network
                            uReceiver = new UnicastReciver(LocalIP, mySocket, peerList, 3);
                            break;
                        } catch (Exception e) {
                        }
                    }
                                                                    
                        inputMessage = "2" + mySocket + publicK +inputMessage;
                        
                        System.out.println("Main/Multicast sender thread: Pedindo o arquivo " + 
                                inputMessage.substring(221) + " no multcast.");                   
                        //Sending a message to the multicast (requesting a file)
                        messageOut = new DatagramPacket(inputMessage.getBytes(), inputMessage.getBytes().length, 
                                group, Integer.parseInt(args[1]));
                        s.send(messageOut);
                                
                        
                
                    }
                }
                s.leaveGroup(group);		
            }catch (SocketException e)
            {
                System.out.println("Socket: " + e.getMessage());
            }catch (IOException e)
            {
                System.out.println("IO: " + e.getMessage());
            }finally 
            {
                if(s != null) 
                    s.close();
            }
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            System.err.println(e.getMessage());
	}
    }
    
    public static boolean portIsOpen(String ip, int port, int timeout) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), timeout);
            socket.close();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    

}


//Class that will be responsable for listen everything on the multcast address
class listener extends Thread 
{ 
    InetAddress group;
    MulticastSocket socket;
    boolean running = true;
    byte[] buffer = new byte[1000];
    String myPublicKey;
    PrivateKey myPrivateKey;
    //List containing the peers info  
    List<PeerInfo> peerList;
    
    public listener (MulticastSocket socket, List<PeerInfo> peerList, String myPublicKey, PrivateKey myPrivateKey) 
    {
        this.myPublicKey = myPublicKey;
        this.peerList = peerList;
        this.socket = socket;
        this.myPrivateKey = myPrivateKey;
        this.start();
    }
    
    @Override
    public void run()
    {
        // an echo server
        DatagramPacket messageIn = new DatagramPacket(buffer, buffer.length);
            
        while(running)
        {
            try 
            {                    
                socket.receive(messageIn);
                if(running)
                       msgInterpreter(messageIn); 
                
            } catch (IOException ex) 
            {
                Logger.getLogger(listener.class.getName()).log(Level.SEVERE, null, ex);
            }

            
            for (int i = 0; i < buffer.length; i++) 
            {
                buffer[i] = 0;
            }
        }     
    }
    private void msgInterpreter(DatagramPacket messageIn)
    {
        //Type 0 = Someone annoucing that joined the multicast 
        
        String msg = new String(messageIn.getData());
        //System.out.println("Received:" + msg);
        
        //Checkinf if the type of the message is zero
        if("1".equals(msg.substring(0,1))){
            
            //System.out.println("Tipo 1 "+msg.substring(1,217));
            if(myPublicKey.equals(msg.substring(1,217))){
                return;
            }
            System.out.println("Multcast thread: Mensagem de entrada identificada, " 
                    + "adicionando novo membro na minha tabela de reputacao, e enviando minha chave. ");
            //Getting the port that came inside the message
            Integer port = Integer.valueOf(msg.substring(217,221));
            //System.out.println("Porta " + port + "porta got:"+ messageIn.getPort());
            
            peerList.add( new PeerInfo(msg.substring(1,217), messageIn.getAddress().getHostAddress(), port, 0) );
            UnicastSender uniSender = new UnicastSender(messageIn.getAddress().getHostAddress(), port, myPublicKey);
        }

        //Checkinf if the type of the message is zero
        if("2".equals(msg.substring(0,1))){
            //System.out.println("Tipo 2 " + msg.substring(5, 221));
            if(myPublicKey.equals(msg.substring(5, 221))){
                return;
            }
            
            
            System.out.println("Multcast thread: Alguem esta pedindo um arquivo.");
            String fileName = msg.substring(221).trim();
               
            try {
            //Getting the jar folder
            String path = listener.class.getProtectionDomain().getCodeSource().
                    getLocation().toURI().getPath().substring(1);
            
            String jarName = path.substring(path.lastIndexOf("/"));
            
            path = path.replace(jarName, "");
            
                System.out.println(path);
            final File folder = new File(path);
            //Verifing if the file is on the folder
            
            
            System.out.println("Multcast thread: Buscando o arquivo com nome: " + fileName);

            
            if(detectFile(folder, fileName))
            {
                System.out.println("Multcast thread: Eu tenho o arquivo, enviando resposta afirmativa.");
                 //Finding the index of the bar so i can get the local ip adress in the string
                String LocalIP = InetAddress.getLocalHost().toString().substring(
                        InetAddress.getLocalHost().toString().indexOf("/") + 1);
                
                int newPort = 0;
                for(int i = 1000; i < 10000; i++){
                    try {
                        DatagramSocket aSocket = null;
                        aSocket = new DatagramSocket(i); 
                        aSocket.disconnect();
                        aSocket.close();
                        newPort = i;
                        UnicastReciver uniReceiver = new UnicastReciver("",newPort,null, 7, myPrivateKey );
                        break;
                    } catch (Exception e) {
                    }
                }

                
                System.out.println("Multcast thread: Aguardando aprovacao para enviar arquivo");
                Integer port = Integer.valueOf(msg.substring(1,5));
                UnicastSender uniSender = new UnicastSender(messageIn.getAddress().getHostAddress(), port, newPort+myPublicKey+fileName);
            }
            else
            {
                System.out.println("Multcast thread: Nao tenho acesso ao arquivo " + fileName);
                        
            }
            
            } catch (NumberFormatException | URISyntaxException | UnknownHostException e) {
                System.out.println("aaaaaaaaaaa");
                System.err.println(e.getMessage());
            } 
            
            
            
            
   
        }
        
    }
    
    private boolean detectFile(final File folder, String fileName)
    {
        for (final File fileEntry : folder.listFiles()) 
        {
            //System.out.println(fileEntry.getName() + " ==" + fileName);
            if(fileEntry.getName().equals(fileName))
                return true;
            
        }
        return false;
    }
    public void exit(int status)
    {
        running = false;
    }
}

//Class that will be responsable for listen everything on the multcast address
class UnicastSender extends Thread 
{
    String ip = null;
    int port = 10000;
    String msg;
     
    
    public UnicastSender (String ip, int port, String msg) 
    {
        this.ip = ip;
        this.port = port;
        this.msg = msg;
        this.start();
    }
    
    public void run()
    {
        System.out.println("Unicast Sender thread: Enviando mensagem para " + ip + ":" + port);
	// args give message contents and destination hostname
	DatagramSocket aSocket = null;
	try {
            aSocket = new DatagramSocket();    
          
            byte [] m = msg.getBytes();
            InetAddress aHost = InetAddress.getByName(ip);	                                                 
            DatagramPacket request = new DatagramPacket(m,  m.length, aHost, port);
            
            //System.out.println("Estou enviando msg para " + ip + ":" + port);
            aSocket.send(request);			                        
	}catch (SocketException e){System.out.println("Socket: " + e.getMessage());
	}catch (IOException e){System.out.println("IO: " + e.getMessage());
	}finally {if(aSocket != null) aSocket.close();}
    }
}

//Class that will be responsable for listen everything on the multcast address
class UnicastReciver extends Thread 
{
    String ip = null;
    int port = 10000;
    boolean running = true;
    List<PeerInfo> peerList;
    int type = 0;
    PrivateKey myPrivateKey;
    String publicKeySender;
    boolean gotBest = false;
    String fileName;
    public UnicastReciver(String ip, int port, List<PeerInfo> peerList, int type) 
    {
        this.ip = ip;
        this.port = port;
        this.peerList = peerList;
        this.type = type;
        this.start();
    }
    public UnicastReciver(String ip, int port, List<PeerInfo> peerList, int type, PrivateKey privateKey) 
    {
        this.ip = ip;
        this.port = port;
        this.peerList = peerList;
        this.type = type;
        this.myPrivateKey = privateKey;
        this.start();
    }
        
    @Override
    public void run()
    {
        if(type == 1)
        {
            System.out.println("Unicast reciever Thread: Aguardando chaves publicas "
                    + "para inicializar minha rep table.");
            DatagramSocket aSocket = null;
            try{
                aSocket = new DatagramSocket(port);  
                aSocket.setSoTimeout(600);
                
                while(true)
                {
                    byte[] buffer = new byte[1000];
                    for (int i = 0; i < buffer.length; i++) {
                       buffer[i] = 0;
                        
                    }
                    DatagramPacket request = new DatagramPacket(buffer, buffer.length);


                    //System.out.println(peerList);
                    aSocket.receive(request);

                    //Received info
                    String receivedInfo = new String(request.getData(), StandardCharsets.UTF_8);                    
                    //PeerInfo receivedData 
                    peerList.add( new PeerInfo(receivedInfo, request.getAddress().getHostAddress(), 
                            request.getPort(), 1)); 
                }
                

            }catch (SocketException e)
            {
                System.out.println("Socket expection: " + e.getMessage());
                if(aSocket != null){
                    aSocket.disconnect();
                    aSocket.close();
                } 
            } catch (IOException e) {
                System.out.println("Unicast reciever Thread: Tabela de reputacao iniciada com sucesso.");
                System.out.println("Unicast reciever Thread: Atual rep table: \n" + peerList);
                if(aSocket != null){
                    aSocket.disconnect();
                    aSocket.close();
                }    
            }finally {if(aSocket != null){
                        aSocket.disconnect();
                        aSocket.close();
            }}
            
            
        }
        else if(type == 3){
            
            System.out.println("Unicast reciever Thread: iniciando o processo de obtencao de arquivos.");
            DatagramSocket aSocket = null;
            
            PeerInfo best = peerList.get(0);
            
            
            try{
                aSocket = new DatagramSocket(port);  
                aSocket.setSoTimeout(1000);
                System.out.println("Unicast reciever Thread: Aguardando respostas dos peers que possuem o arquivo.");
                while(true)
                {
                    byte[] buffer = new byte[1000];
                    for (int i = 0; i < buffer.length; i++) {
                       buffer[i] = 0;
                        
                    }
                    DatagramPacket request = new DatagramPacket(buffer, buffer.length);


                    //System.out.println(peerList);
                    aSocket.receive(request);
                    //Received info
                    String receivedData = new String(request.getData(), StandardCharsets.UTF_8);                    
                    
                    //Checking if the new reputation is better than actual
                    for (int i = 0; i < peerList.size(); i++) {
     
                        if(peerList.get(i).publicKey.trim().equals(receivedData.substring(4,220).trim()))
                        {
                            if (peerList.get(i).reputacao >= best.reputacao) {
                                best = peerList.get(i);
                                best.socket = Integer.valueOf( receivedData.substring(0,4));
                                this.gotBest = true;
                                //Saving the public key
                                this.publicKeySender = receivedData.substring(4,220).trim();
                                //Saving the filename
                                this.fileName = receivedData.substring(220);
                            }
                            break;
                        }
                    }
                }

                }catch (SocketException e)
                {
                    System.out.println("Socket expection: " + e.getMessage());
                    if(aSocket != null){
                        aSocket.disconnect();
                        aSocket.close();
                    } 
                } catch (IOException e) {
                    if(!this.gotBest)
                    {
                        System.out.println("Unicast reciever Thread: Ninguem tem esse arquivo.");
                        return;
                    }   
                    UnicastSender uniSender = new UnicastSender(ip, best.socket, port+fileName );
                    
                    //Receiving the file
                    System.out.println("Unicast reciever Thread: Peer escolhido, aguardando arquivo.");
                     byte[] buffer = new byte[1000];
                    for (int i = 0; i < buffer.length; i++) {
                       buffer[i] = 0;
                        
                    }
                    DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                    
                try {
                    aSocket.receive(request);
                
                    System.out.println("Unicast reciever Thread: Arquivo recebido com sucesso.");
                    String path = listener.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
                    path = path.substring(1);
                    String jarName = path.substring(path.lastIndexOf("/"));
            
                    path = path.replace(jarName, "");
                    
                    Path path2save= Paths.get(path+ "/recieved_"+fileName.trim());
                    
                    
                    //Descriptografando
                    System.out.println("Unicast reciever Thread: Descriptografando o arquivo.");
                    AsymmetricCryptography ac = new AsymmetricCryptography();
                    
                    byte[] publicBytes = Base64.decodeBase64(publicKeySender);
                    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
                    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                    PublicKey pubKey = keyFactory.generatePublic(keySpec);

                    String receivedFile = new String(request.getData(), StandardCharsets.UTF_8);
                    
                    String output = ac.decryptText(receivedFile, pubKey);
                    
                    System.out.println("Unicast reciever Thread: Salvando o arquivo no endereco: " 
                            + path+ "/recieved_"+fileName.trim());
                    Files.write( path2save , output.getBytes());

                    
                    if(aSocket != null){
                        aSocket.disconnect();
                        aSocket.close();
                    } 
                    return;
                 } catch (IOException ex) {
                    System.out.println("Unicast reciever Thread: Nao foi possivel obter o arquivo tente novamente.");
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchPaddingException ex) {
                    Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidKeySpecException ex) {
                    Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidKeyException ex) {
                    System.out.println("Unicast reciever Thread: Ocorreu algum erro "
                            + "ao descriptografar o arquivo, tente novamente");
                } catch (IllegalBlockSizeException ex) {
                    Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
                } catch (BadPaddingException ex) {
                    Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
                } catch (URISyntaxException ex) {
                    Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
                }   
                }finally {if(aSocket != null){
                            aSocket.disconnect();
                            aSocket.close();
                }}
                        
        }
        else if(type == 7)
        {
            
            DatagramSocket aSocket = null;
            try{
                aSocket = new DatagramSocket(port); 
                System.out.println("Unicast reciever Thread: Aguardando para ver se sou o peer escolhido.");
                aSocket.setSoTimeout(8000);

                byte[] buffer = new byte[1000];
                for (int i = 0; i < buffer.length; i++) {
                       buffer[i] = 0;
                        
                    }
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);


                //Checking if i need to send the file
                aSocket.receive(request);
                String receivedData = new String(request.getData(), StandardCharsets.UTF_8);
                //Getting the jar folder
                String path = listener.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
                
                String jarName = path.substring(path.lastIndexOf("/"));
            
                path = path.replace(jarName, "");
                final File folder = new File(path);
                
                Path path2= Paths.get(path.substring(1)+ "\\" +receivedData.substring(4).trim());
                byte[] data = Files.readAllBytes(path2);

                int port2SendFile = Integer.valueOf(receivedData.substring(0,4));
                System.out.println("Unicast reciever Thread:  Enviando arquivo para o peer pedido.");
                String sendData = new String(data, StandardCharsets.UTF_8);
                
                AsymmetricCryptography ac = new AsymmetricCryptography();
                
                String encrypted_msg = ac.encryptText(sendData, myPrivateKey);
                
                UnicastSender uniSender = new UnicastSender(request.getAddress().getHostAddress(), port2SendFile,encrypted_msg);
               
                return;
                

            }catch (SocketException e)
            {
                System.out.println("Unicast reciever Thread: Ocorreu um erro na criacao do "
                        + "socket de espera de resposta, tente novamente."
                        + " Ocorre por causa de sincronia(como resolver).");
                if(aSocket != null){
                    aSocket.disconnect();
                    aSocket.close();
                } 
            } catch (IOException e) {
                //System.out.println("Unicast reciever Thread: Tempo limite de espera alcancado.");
                System.out.println("Unicast reciever Thread: Erro " + e.getMessage());
                if(aSocket != null){
                    aSocket.disconnect();
                    aSocket.close();
                }    
            } catch (URISyntaxException ex) {
                Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchPaddingException ex) {
                Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalBlockSizeException ex) {
                Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BadPaddingException ex) {
                Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(UnicastReciver.class.getName()).log(Level.SEVERE, null, ex);
            }finally {if(aSocket != null){
                        aSocket.disconnect();
                        aSocket.close();
            }}
        }
                
    }

    public void exit(int status)
    {
        running = false;
    }
}
